import colors from 'vuetify/es5/util/colors';
export default {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [{
      charset: 'utf-8'
    }, {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1'
    }, {
      hid: 'description',
      name: 'description',
      content: process.env.npm_package_description || ''
    }],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }],
    script: [{
      "src": "https://cdn.polyfill.io/v2/polyfill.min.js?features=Element.prototype.classList"
    }, {
      "src": "https://cdn.jsdelivr.net/npm/focus-visible@5.0.2/dist/focus-visible.min.js"
    }]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#fff'
  },

  /*
  ** Global CSS
  */
  css: ["~assets/layouts/global.css", "vue-essential-slices/src/styles/styles.scss"],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: ["~plugins/fireauth.js"],

  /*
  ** Nuxt.js dev-modules
  */
  buildModules: ['@nuxtjs/vuetify'],

  /*
  ** Nuxt.js modules
  */
  modules: ['@nuxtjs/pwa', ["@nuxtjs/prismic", {
    "endpoint": "https://getblog.cdn.prismic.io/api/v2",
    "apiOptions": {
      "routes": [{
        "type": "page",
        "path": "/:uid"
      }]
    }
  }], ["nuxt-sm"]],

  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: "e32018",
          accent: "d6d6dc",
          secondary: "d6d6dc",
          info: "F1FAEE",
          warning: "455874",
          error: "E63946",
          success: "A8DADC"
        }
      }
    }
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {},

    transpile: ["vue-slicezone", "nuxt-sm"]
  }
};